import 'package:flutter/material.dart';

class ForgetPage extends StatefulWidget {
  @override
  _ForgetPageState createState() => _ForgetPageState();
}

class _ForgetPageState extends State<ForgetPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(),
      body: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 1),
            child: pageView(context),
          ),
        ),
        // masterButton(),
      ]),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Card(
            margin: EdgeInsets.all(10),
            elevation: 5.0,
            child: Container(
              padding: EdgeInsets.all(10),
              child: Column(
                children: <Widget>[
                  TextField(
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Username", hasFloatingPlaceholder: true),
                  ),
                  TextField(
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w600,
                        fontFamily: "Sans"),
                    decoration: InputDecoration(
                        labelText: "Phone number",
                        hasFloatingPlaceholder: true),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20),
                  ),
                  masterButton(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  SizedBox masterButton() {
    return SizedBox(
      width: double.infinity,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(0.0))),
        height: 50,
        color: Colors.black87,
        child: Text(
          'Change Password',
          style: TextStyle(
              color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),
        ),
        onPressed: () {
          Navigator.of(context)
              .pushNamedAndRemoveUntil("/login", ModalRoute.withName('/'));
        },
      ),
    );
  }

  AppBar mainAppBar() {
    return AppBar(
      title: Text("Forget Password"),
    );
  }
}
