import 'package:eaasy_tickets/services/authservice.dart';
import 'package:flutter/material.dart';




class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
//   TextEditingController _usernameController = TextEditingController();
//   TextEditingController _passwordController = TextEditingController();

// final AuthService _authService = new AuthService();

//   void login() async {
//     _authService.login(
//         context, _usernameController.text, _passwordController.text);
//   }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/bg.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(),
            Image.asset("assets/images/logo.png"),
            Card(
              color: Color.fromRGBO(255, 255, 255, 0.75),
              margin: EdgeInsets.only(bottom: 20),
              elevation: 10,
              child: Container(
                padding: EdgeInsets.all(15),
                width: MediaQuery.of(context).size.width - 20,
                child: Column(
                  children: <Widget>[
                    TextField(
                      // controller: _usernameController,
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Sans"),
                      decoration: InputDecoration(
                          labelText: "Username", hasFloatingPlaceholder: true),
                    ),
                    TextField(
                      // controller: _passwordController,
                      obscureText: true,
                      style: TextStyle(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          fontFamily: "Sans"),
                      decoration: InputDecoration(
                          labelText: "Password", hasFloatingPlaceholder: true),
                    ),
                    Container(
                      height: 15,
                    ),
                    SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        color: Colors.black87,
                        onPressed: () {
                          // this.loginPage();
                          Navigator.of(context).pushNamedAndRemoveUntil(
                              "/app", ModalRoute.withName('/'));
                        },
                        child: Text(
                          "Login",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pushNamed("/create");
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text("Create Account"),
                          ),
                        ),
                        InkWell(
                            onTap: () {
                              Navigator.of(context).pushNamed("/forget");
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text("Forget Password"),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  // void loginPage() {
  //   debugPrint(_usernameController.text);
  //   debugPrint(_passwordController.text);
  // }
}