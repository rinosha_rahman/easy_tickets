import 'dart:async';

import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  void initState() {
    Timer(Duration(seconds: 3), () {
      Navigator.of(context).pushReplacementNamed("/login");
    });
    super.initState();
  }
  // nextPage() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   debugPrint("isLogged : " + prefs.getBool('isLogged').toString());
  //   if (null != prefs.getBool('isLogged') && prefs.getBool('isLogged')) {
  //     Navigator.of(context).pushReplacementNamed("/app");
  //   } else {
  //     Navigator.of(context).pushReplacementNamed("/login");
  //   }
  // }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Container(),
              Image.asset(
                "assets/images/logo.png",
              ),
              Text(
                "1.0.0.0",
                style: TextStyle(color: Colors.black45),
              ),
            ]),
      ),
    );
  }
}
