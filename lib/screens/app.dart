import 'package:eaasy_tickets/screens/home.dart';
import 'package:eaasy_tickets/screens/profile.dart';
import 'package:eaasy_tickets/screens/trip.dart';
import 'package:eaasy_tickets/screens/wallet.dart';
import 'package:flutter/material.dart';

class AppPage extends StatefulWidget {
  @override
  _AppPageState createState() => _AppPageState();
}

class _AppPageState extends State<AppPage> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return PageView(
      children: <Widget>[
        Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.white,
          appBar: mainAppBar(),
          bottomNavigationBar: mainBottomNavigationBar(),
          body: buildPageView(),
        )
      ],
    );
  }

  PageController pageController =
      PageController(initialPage: 0, keepPage: true);

  BottomNavigationBar mainBottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: index,
      onTap: (int index) {
        bottomTapped(index);
      },
      type: BottomNavigationBarType.fixed,
      items: buildBottomNavigationBarItems(),
    );
  }

  List<BottomNavigationBarItem> buildBottomNavigationBarItems() {
    return [
      BottomNavigationBarItem(icon: Icon(Icons.home), title: Text("Home")),
      BottomNavigationBarItem(
          icon: Icon(Icons.account_balance_wallet), title: Text("Wallet")),
      BottomNavigationBarItem(
          icon: Icon(Icons.directions_bus), title: Text("Trip")),
      BottomNavigationBarItem(icon: Icon(Icons.person), title: Text("Profile")),
    ];
  }

  void bottomTapped(int index) {
    setState(() {
      this.index = index;
      pageController.animateToPage(index,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    });
  }

  AppBar mainAppBar() {
    return AppBar(
      title: Text("Welcome Rinosha,"),
      actions: <Widget>[
        IconButton(
            icon: Icon(Icons.help),
            onPressed: () {
              Navigator.of(context).pushNamed("/help");
            }),
        IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              Navigator.of(context)
                  .pushNamedAndRemoveUntil("/login", ModalRoute.withName('/'));
            }),
      ],
    );
  }

  buildPageView() {
    return PageView(
      controller: pageController,
      onPageChanged: (index) {
        pageChanged(index);
      },
      children: <Widget>[
        HomePage(),
        WalletPage(),
        TripPage(),
        ProfilePage(),
      ],
    );
  }

  void pageChanged(int index) {
    setState(() {
      this.index = index;
    });
  }
}
