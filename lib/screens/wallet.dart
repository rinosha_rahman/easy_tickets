import 'package:flutter/material.dart';

class WalletPage extends StatefulWidget {
  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 0),
            child: pageView(context),
          ),
        ),
      ]),
    );
  }

  Card itemList(String amount, String date, String time, String status) {
    return Card(
      margin: EdgeInsets.only(left: 10, right: 10, top: 5),
      elevation: 5,
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10, top: 10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(date),
                Text(time),
              ],
            ),
          ),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(
                    left: 10.0, right: 10, top: 10, bottom: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      amount,
                      style:
                          TextStyle(fontWeight: FontWeight.w900, fontSize: 18),
                    ),
                    Text(
                      status,
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color:
                            status == "Completed" ? Colors.green : Colors.red,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Card(
            margin: EdgeInsets.only(left: 10, right: 10, top: 10, bottom: 5),
            elevation: 5,
            child: Padding(
              padding: const EdgeInsets.only(
                  top: 10.0, left: 10, right: 10, bottom: 5),
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: Text(
                      "INR 559.50",
                      style:
                          TextStyle(fontWeight: FontWeight.w900, fontSize: 30),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: Text("Last top up on 25th February 2020"),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10.0),
                    child: SizedBox(
                      width: double.infinity,
                      child: RaisedButton(
                        color: Colors.black87,
                        onPressed: () {
                          Navigator.of(context).pushReplacementNamed("/wallet");
                        },
                        child: Text(
                          "Recharge now",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          itemList("INR 250.00", "25-02-2020", "2:30 PM", "Completed"),
          itemList("INR 250.00", "25-02-2020", "2:40 PM", "Completed"),
          itemList("INR 250.00", "25-02-2020", "2:50 PM", "Failed"),
          itemList("INR 250.00", "25-02-2020", "2:51 PM", "Completed"),
          itemList("INR 250.00", "25-02-2020", "2:52 PM", "Failed"),
          itemList("INR 250.00", "25-02-2020", "2:53 PM", "Failed"),
          itemList("INR 250.00", "25-02-2020", "2:54 PM", "Completed"),
          itemList("INR 250.00", "25-02-2020", "2:55 PM", "Completed"),
          itemList("INR 250.00", "25-02-2020", "2:56 PM", "Failed"),
          itemList("INR 250.00", "25-02-2020", "2:57 PM", "Completed"),
          Padding(
            padding: EdgeInsets.only(bottom: 25),
          )
        ],
      ),
    );
  }
}
