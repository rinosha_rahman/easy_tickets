import 'package:flutter/material.dart';

class CreatePage extends StatefulWidget {
  @override
  _CreatePageState createState() => _CreatePageState();
}

class _CreatePageState extends State<CreatePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: mainAppBar(),
      body: Column(children: <Widget>[
        Expanded(
          child: Padding(
            padding: EdgeInsets.only(left: 0, right: 0, top: 1),
            child: pageView(context),
          ),
        ),
        masterButton(),
      ]),
    );
  }

  SingleChildScrollView pageView(context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          // DESIGN ITEMS GOES HERE
          itemBox("Your Name"),
          itemBox("Username"),
          itemBox("Password"),
          itemBox("Confirm Password"),
          itemBox("Mobile Number"),
          itemBox("Address Line 1"),
          itemBox("Address Line 2"),
        ],
      ),
    );
  }

  Center itemBox(String title) {
    return Center(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0.0),
        child: new TextField(
          decoration: new InputDecoration(labelText: title),
        ),
      ),
    );
  }

  SizedBox masterButton() {
    return SizedBox(
      width: double.infinity,
      child: MaterialButton(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(0.0))),
        height: 50,
        color: Colors.black87,
        child: Text(
          'SIGN IN',
          style: TextStyle(
              color: Colors.white, fontSize: 15, fontWeight: FontWeight.w600),
        ),
        onPressed: () {
          Navigator.of(context)
              .pushNamedAndRemoveUntil("/app", ModalRoute.withName('/'));
        },
      ),
    );
  }

  AppBar mainAppBar() {
    return AppBar(
      title: Text("Create new account"),
    );
  }
}
